const express = require("express")
const userModel = require("./model")
const connectDataBase = require("./controller")

const app = express()
app.use(express.json())

app.get("/", (req, res) => {
    res.send("Home Page")
});

app.get("/getuser", async (req, res) => {
    const dataArr = await userModel.find();
    let html = `<ol>
                ${dataArr.map((elem, indx) => {
        return `<li> ${elem.first_name + " : " + elem.email} </li>`
    })}</ul>`
    res.send(html)
    // console.log(req.params)
    // console.log(req.body)
    // console.log(req.headers)
    // console.log(res)
});

app.post("/createuser", async (req, res) => {
    const body = req.body
    if (!body.first_name || !body.email) {
        return res.status(400).json({ msg: "all fileds are required" })
    }
    const result = await userModel.create({
        first_name: body.first_name,
        email: body.email
    });
    return res.status(201).json("user created " + result);
});

app.put("/updateuser", async (req, res) => {
    const body = req.body
    if (!body.first_name || !body.new_name || !body.new_email) {
        return res.status(400).json({ msg: "all fileds are required" })
    }
    const result = await userModel.updateOne({ first_name: body.first_name },
        {
            $set: {
                'first_name': body.new_name,
                'email': body.new_email
            }
        }, { upsert: true })
    return res.status(201).json("user Updated " + { result });
});

app.delete("/deleteuser", async (req, res) => {
    const body = req.body
    if (!body.first_name) {
        return res.status(400).json({ msg: "all fileds are required" })
    }
    const result = await userModel.deleteOne({ first_name: body.first_name })
    return res.status(201).json("user removed : " + { result });
});

app.listen(3000, () => {
    console.log("server is running...")
    connectDataBase()
})