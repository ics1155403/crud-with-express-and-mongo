const mongoose = require("mongoose")

// creating schema for database
let dataSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
    }
})

// on basis of schema create model
const userModel = mongoose.model("userModel", dataSchema)

module.exports = userModel;