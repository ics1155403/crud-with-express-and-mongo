const mongoose = require("mongoose")

// connecting mongodb
function connectDataBase(){

    mongoose.connect("mongodb://localhost:27017/ICSdata")
    .then(() => { console.log("Database connection succesfull...") })
    .catch((err) => { console.log("Error occured : ", err) })
    
}
module.exports = connectDataBase;